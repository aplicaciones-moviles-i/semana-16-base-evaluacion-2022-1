package facci.pm.recyclerviewoverview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import facci.pm.recyclerviewoverview.adapters.TeamsAdapter;
import facci.pm.recyclerviewoverview.interfaces.RecyclerViewItemClickInterface;
import facci.pm.recyclerviewoverview.models.Team;

public class MainActivity extends AppCompatActivity implements RecyclerViewItemClickInterface {

    RecyclerView teamsRecyclerView;

    List<Team> dummyTeamListData;

    private void FillList(){
        dummyTeamListData = new ArrayList<>();
        dummyTeamListData.add(new Team(111,"https://upload.wikimedia.org/wikipedia/commons/0/0b/EscudoCSEmelec.png","Emelec S.C."));
        dummyTeamListData.add(new Team(345,"https://a.espncdn.com/combiner/i?img=/i/teamlogos/soccer/500/6017.png","Aucas F.C."));
        dummyTeamListData.add(new Team(999,"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Escudo_del_Manta_FC.png/147px-Escudo_del_Manta_FC.png","Manta F.C."));
        dummyTeamListData.add(new Team(222,"https://upload.wikimedia.org/wikipedia/commons/0/0b/EscudoCSEmelec.png","Emelec S.C."));
        dummyTeamListData.add(new Team(890,"https://a.espncdn.com/combiner/i?img=/i/teamlogos/soccer/500/6017.png","Aucas F.C."));
        dummyTeamListData.add(new Team(238,"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Escudo_del_Manta_FC.png/147px-Escudo_del_Manta_FC.png","Manta F.C."));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        teamsRecyclerView = findViewById(R.id.RecyclerViewTeams);

        //1 OBTENER LA DATA DE LA FUENTE DE DATOS, POR EJEMPLO,
        //DE UN SERVICIO WEB REST
        FillList();
        //2 DEFINIR EL ESTILO VISUAL
        teamsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        //3 ADAPTAR LOS DATOS AL RECYCLERVIEW
        TeamsAdapter teamsAdapter = new TeamsAdapter(dummyTeamListData, this);
        //4 ASIGNAR EL ADAPTADOR
        teamsRecyclerView.setAdapter(teamsAdapter);

    }

    @Override
    public void onItemClick(int position) {
        //SE CAPTURA EL ID DE LA POSICIÓN DEL RECYCLER VIEW
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        Log.e("RECYCLERVIEW POSITION", String.valueOf( position));
        //SE CONSULTA EL ID DEL ITEM A TRAVES DE LA POSICION DEL ITEM EN EL RECYCLERVIEW
        //Y SE ENVÍA A LA SIGUIENTE PANTALLA A TRAVES DE PUTEXTRA
        intent.putExtra("item_id", dummyTeamListData.get(position).getId());
        startActivity(intent);

    }
}