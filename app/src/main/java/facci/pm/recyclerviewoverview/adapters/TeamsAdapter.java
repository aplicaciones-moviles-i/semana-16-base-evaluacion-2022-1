package facci.pm.recyclerviewoverview.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.List;
import facci.pm.recyclerviewoverview.R;
import facci.pm.recyclerviewoverview.interfaces.RecyclerViewItemClickInterface;
import facci.pm.recyclerviewoverview.models.Team;
import facci.pm.recyclerviewoverview.view_holders.TeamViewHolder;

public class TeamsAdapter extends RecyclerView.Adapter<TeamViewHolder> {

    private final RecyclerViewItemClickInterface recyclerViewItemClickInterface;
    List<Team> teamListData;

    public TeamsAdapter(List<Team> teamListData, RecyclerViewItemClickInterface recyclerViewItemClickInterface) {
        this.teamListData = teamListData;
        this.recyclerViewItemClickInterface = recyclerViewItemClickInterface;
    }

    @Override
    public TeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_item_layout, null, false);
        return new TeamViewHolder(view, recyclerViewItemClickInterface);
    }

    @Override
    public void onBindViewHolder(TeamViewHolder holder, int position) {
        holder.teamNameTextView.setText(teamListData.get(position).getName());
        Picasso.get()
                .load(teamListData.get(position).getImageURL())
                .into(holder.teamImageView);
    }

    @Override
    public int getItemCount() {
        return teamListData.size();
    }


}
