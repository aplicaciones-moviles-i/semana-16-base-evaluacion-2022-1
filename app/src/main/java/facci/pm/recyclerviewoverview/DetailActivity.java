package facci.pm.recyclerviewoverview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //SE OBTIENE EL ITEM PASADO DE LA PANTALLA ANTERIOR CON getIntExtra
        int id = getIntent().getIntExtra("item_id", 0);
        //SE REALIZA LO QUE SE NECESITE CON EL ID DEL ITEM, POR EJEMPLO,
        //UNA CONSULTA INDIVIDUAL DE DATOS AL SERVIDOR
        Toast.makeText(this, String.valueOf( id), Toast.LENGTH_SHORT).show();
    }
}