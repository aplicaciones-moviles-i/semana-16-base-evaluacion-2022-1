package facci.pm.recyclerviewoverview.view_holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import facci.pm.recyclerviewoverview.R;
import facci.pm.recyclerviewoverview.interfaces.RecyclerViewItemClickInterface;

public class TeamViewHolder extends RecyclerView.ViewHolder {

    public ImageView teamImageView;
    public TextView teamNameTextView;

    public TeamViewHolder(View itemView,
                          RecyclerViewItemClickInterface recyclerViewItemClickInterface) {
        super(itemView);
        teamImageView = itemView.findViewById(R.id.teamImageView);
        teamNameTextView = itemView.findViewById(R.id.teamNameTextView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recyclerViewItemClickInterface != null){
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION){
                        recyclerViewItemClickInterface.onItemClick(pos);
                    }
                }
            }
        });
    }
}
