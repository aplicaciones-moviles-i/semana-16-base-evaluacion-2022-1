package facci.pm.recyclerviewoverview.interfaces;

public interface RecyclerViewItemClickInterface {

    void onItemClick(int position);
}
