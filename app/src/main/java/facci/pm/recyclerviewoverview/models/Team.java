package facci.pm.recyclerviewoverview.models;

public class Team {

    private int Id;
    private String ImageURL;
    private String Name;

    public Team(int id, String imageURL, String name) {
        this.Id = id;
        this.ImageURL = imageURL;
        this.Name = name;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
